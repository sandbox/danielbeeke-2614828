<?php

/**
 * @file
 * Contains \Drupal\views\Plugin\views\filter\veefc.
 */

namespace Drupal\veefc\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\Views;

/**
 * Simple filter to handle matching of multiple options selectable via checkboxes
 *
 * Definition items:
 * - options callback: The function to call in order to generate the value options. If omitted, the options 'Yes' and 'No' will be used.
 * - options arguments: An array of arguments to pass to the options callback.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("veefc")
 */
class veefc extends InOperator {

    /**
     * Overrides \Drupal\views\Plugin\views\filter\FilterPluginBase::init().
     */
    public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL) {
        parent::init($view, $display, $options);

        // TODO Yikes.. fix this shit.
        $this->realField = str_replace('veefc', 'target_id', $this->realField);
    }

    protected function defineOptions() {
        $options = parent::defineOptions();
        $options['entity_reference_view']['default'] = NULL;
        return $options;
    }

    protected function valueForm(&$form, FormStateInterface $form_state) {
        if (!$form_state->get('exposed')) {
            $displays = Views::getApplicableViews('entity_reference_display');
            // Filter views that list the entity type we want, and group the separate
            // displays by view.
            $entity_type = \Drupal::entityManager()->getDefinition($this->getEntityType());
            $view_storage = \Drupal::entityManager()->getStorage('view');

            $options = array();
            foreach ($displays as $data) {
                list($view_id, $display_id) = $data;
                $view = $view_storage->load($view_id);
                if (in_array($view->get('base_table'), [$entity_type->getBaseTable(), $entity_type->getDataTable()])) {
                    $display = $view->get('display');
                    $options[$view_id . ':' . $display_id] = $view_id . ' - ' . $display[$display_id]['display_title'];
                }
            }

            $form['entity_reference_view'] = array(
                '#type' => 'select',
                '#required' => TRUE,
                '#default_value' => isset($this->options['entity_reference_view']) ? $this->options['entity_reference_view'] : NULL,
                '#options' => $options,
                '#title' => $this->t('The view used to generate the select list of the exposed filter')
            );
        }

        parent::valueForm($form, $form_state);
    }

    /**
     * Child classes should be used to override this function and set the
     * 'value options', unless 'options callback' is defined as a valid function
     * or static public method to generate these values.
     *
     * This can use a guard to be used to reduce database hits as much as
     * possible.
     *
     * @return
     *   Return the stored values in $this->valueOptions if someone expects it.
     */
    public function getValueOptions() {
        $view_to_get_options = explode(':', $this->options['entity_reference_view']);
        $view = Views::getView($view_to_get_options[0]);

        $this->valueOptions = [];

        if (is_object($view)) {
            $view->setDisplay($view_to_get_options[1]);
            $view->preview = TRUE;
            $output = $view->display_handler->preview();
            $view->postExecute();

            foreach ($output as $row) {
                $this->valueOptions[$row['#row']->nid] = \Drupal::service('renderer')->renderPlain($row);
            }
        }

        return $this->valueOptions;
    }
}